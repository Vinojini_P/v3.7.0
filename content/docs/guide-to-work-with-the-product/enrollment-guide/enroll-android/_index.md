---
bookCollapseSection: true
weight: 1
---

# Enroll Android

This section describes how each device type can be enrolled. Prior to this section, make sure to follow  [install agent]({{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/install-agent/) section
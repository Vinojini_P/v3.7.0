---
bookCollapseSection: true
weight: 1
---

# General Platform COnfigurations

Configure how often the devices enrolled with Entgra IoT Server need to be monitored via the general platform configurations and deploy the geo analytics artifacts required for location based services in a multi tenant environment using the general platform configurations.

Follow the instructions below to configure the general platform settings:

1.  [Sign in to the Entgra IoTS device management console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/) and click the menu icon.

    <img src = "352821220.png" style="border:5px solid black ">
    
2.  Click **Platform Configurations**.

    <img src = "352821214.png" style="border:5px solid black ">
    
3.  Define the **Monitoring Frequency** in seconds, to monitor the enforced policies on the devices, and click  **SAVE**.  
    
    <img src = "352821230.png" style="border:5px solid black ">
    
4.  The geo analytics artifacts are deployed by default in the Entgra IoT Server super tenant. However, if you are setting up geofencing or location based services in a multi-tenant environment, you have to deploy the geo analytics artifacts in each tenant. 

    1.Click the menu icon.
    
    <img src = "../../../image/4000.png" style="border:5px solid black ">
    
    2.Click CONFIGURATION MANAGEMENT
    
    <img src = "../../../image/4001.png" style="border:5px solid black ">
    
    3.Click PLATFORM CONFIGURATIONS
    
    <img src = "../../../image/4004.png" style="border:5px solid black ">

    4.  Click the **Deploy Geo Analytics Artifacts** button. You can also use this button to 
        re-deploy the geo analytics artifacts in super tenant mode if required.   
        
        <img src = "352821235.png" style="border:5px solid black ">

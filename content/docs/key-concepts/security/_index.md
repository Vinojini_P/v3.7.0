---
bookCollapseSection: true
weight: 9
---

# Security

Security refers to the means through which computer systems are protected from damage and disruption without being compromised to risks and vulnerabilities. Entgra IoT Server implements security at the application level and transport level.

## Application-level Security

Application-level security refers to the security requirements at the application level. Following is a list of concepts related to application-level security:

### Encryption

Encryption is the process of translating/encoding data/messages (plaintext) using an algorithm (cipher) into a secret code (cipher text) that can only be accessed by authorized entities with a secret key or a password. 

### Authentication

Authentication is the process used to distinctly identify a certain entity using the following factors:

*   **Knowledge factor**: This is something the user knows, e.g., password, PIN, and security question.
*   **Ownership factor**: This is something the user has, e.g., identity card, mobile phone, and security token.
*   **Inherence factor**: This is something the user is/does, i.e., biometrics. 

Authentication is implemented in either of the following forms:

*   **Single-factor authentication**: This mechanism utilizes a single factor to authenticate an entity.
*   **Two-factor authentication**: This mechanism utilizes two factors to authenticate an entity, e.g., password and security token.
*   **Multi-factor authentication**: This mechanism utilizes more than two factors to authenticate an entity.

Entgra IoT Server uses OAuth, Basic Auth, JWT, and [mutual SSL]({{< param doclink >}}using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication) for authentication.

### Authorization

Authorization is the process via which an entity is granted permission to access to another entity, e.g., data, resources, system. In general, authorization takes places subsequent to authentication. Entgra IoT Server uses Role-based access control (RBAC) and scopes to implement authorization.

### Certificates

A certificate (also known as SSL certificate) is an encryption tool issued by a trusted certification authority (CA) that encrypts data transmitted between a client and a server. Entgra IoT Server uses Simple Certificate Enrollment Protocol (SCEP) to securely enroll and authenticate iOS devices by creating a certificate for each device. For more information, [Certificate-based Authentication](http://wso2.com/library/articles/2017/09/securing-device-to-IoT-platform-communication/#certificatebasedauth) and [Certificate Management]({{< param doclink >}}using-entgra-iot-server/product-administration/certificate-management/).


### Tokens

A token is a credential created by an authentication server that grants an entity to access protected resources. Entgra IoT Server users tokens to identify devices and their ability to access protected resources. For more information, see [Token-Based Authentication](http://wso2.com/library/articles/2017/09/securing-device-to-IoT-platform-communication/#tokenbasedauth) and [Generating the Access Token]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/device-management-rest-apis/#generating-the-access-token).

### Scopes

Scopes define the permission model that enables invoking an API. For more information, see [Getting the Scope Details of an API]({{< param doclink >}}tutorials/getting-started-with-apis/getting-the-scope-details-of-an-api/) and [Device Management API Scopes]({{< param doclink >}}tutorials/getting-started-with-apis/device-management-api-scopes/). 

### Single Sign-On

Single sign-on (SSO) enables users to provide their credentials once and obtain access to multiple applications. A user who has already signed in to an application is not prompted for credentials to access other applications until that session terminates. 

Once signed in to an application, users are not prompted for their credentials to access other applications until the session terminates. Entgra IoT Server enables SSO for its web applications, i.e., Device Management Console, API Store, App Publisher, and App Store. 

### Role-based Access Control

Role-based access control (RBAC) is a type of access control that restricts access to authorized users based on their role.

## Transport-level Security

Transport-level security (TLS) is a mechanism that secures internet and intranet communications. Entgra IoT Server uses [mutual SSL]({{< param doclink >}}using-entgra-iot-server/product-administration/certificate-management/#mutual-ssl-authentication), certificates, and keystores to implement transport-level security. 


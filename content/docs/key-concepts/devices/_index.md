---
bookCollapseSection: true
weight: 1
---

# Devices 

A device is a physical computing unit capable of achieving one or multiple tasks. Entgra IoT Server enables organizations to enroll, secure, manage, and monitor devices, irrespective of the mobile operator, service provider, or the organization.

## Device Ownership 

In some corporate environments, mobile devices are used to carry out organizational tasks such as email access. These devices are categorized into two main groups based on the ownership:

*   **Bring Your Own Device (BYOD)**: These devices are owned by the employee and managed by the employer. They are subject to policies and conventions enforced by the employer.
*   **Corporate Owned, Personally Enabled (COPE)**: These devices are owned and managed by the employer. 

## Device Types 

Devices are subdivided into two main groups based on the usage: 

*   **Mobile devices**: These are handheld devices that are usually used for day-to-day ordinary activities such as making phone calls, sending emails, and setting up alarms. Entgra IoT Server supports managing Android, iOS and Windows mobile devices. 

    *   For a quick hands-on experience see [Mobile Device and App Management]({{< param doclink >}}quick-start-guide/).
    *   You can also try enrolling [Android]({{< param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-android/), [iOS]({{< 
    param doclink >}}guide-to-work-with-the-product/enrollment-guide/enroll-ios/), and [Windows]({{< param doclink >}}tutorials/windows/). If you do not have an Android device to enroll with Entgra IoT Server, see [Android Virtual Device]({{< param doclink >}}tutorials/android-virtual-device/).
    *   You can try adding operations and policies to [Android]({{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/), [iOS]({{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/), and [Windows]({{< param doclink >}}tutorials/windows/).
    *   For detailed information on managing mobile devices, see the following sections:

        *   [Working with Android Devices]({{< param doclink >}}using-entgra-iot-server/Working-with-Android-Devices/)

        *   [Working with iOS Devices]({{< param doclink >}}using-entgra-iot-server/working-with-ios/)

        *   [Working with Windows Devices]({{< param doclink >}}using-entgra-iot-server/working-with-windows-devices/).

    

*   **IoT devices**: These devices are specifically created to function in a connected environment via the internet. They can collect data via embedded sensors and exchange them with other devices. Entgra IoT Server supports managing Android Sense, Arduino, Raspberry Pi and custom IoT device types. 

    <!-- <div class="confluence-information-macro confluence-information-macro-information"> -->

    <!-- <div class="confluence-information-macro-body"> -->


    
    *   For a quick hands-on experience, see [Enterprise IoT solution](https://docs.wso2.com/display/IoTS310/Enterpris +IoT+solution). 
    *   You can also try the available samples for [Android Sense](https://docs.wso2.com/display/IoTS310/Android+Sense), [Arduino](https://docs.wso2.com/display/IoTS310/Arduino), and [Raspberry Pi](https://docs.wso2.com/display/IoTS310/Raspberry+Pi). If the available IoT device types do not meet your requirement, see [Creating a New Device Type]({{< param doclink >}}tutorials/creating-a-new-device-type/), and [Device Manufacturer Guide]({{< param doclink >}}using-entgra-iot-server/device-manufacturer-guide/).
    

## Device Groups

Entgra IoT Server allows you to [group]({{< param doclink >}}product-administration-guide/manage-groups/) multiple enrolled devices in order to monitor multiple devices in one go. 